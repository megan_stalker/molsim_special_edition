import matplotlib.pyplot as plt
import csv 
from matplotlib.backends.backend_pdf import PdfPages

x1 = []
y1 = []

with open('lam_4_1.csv', 'r') as csvfile:
	plots = csv.reader(csvfile, delimiter=',')
	for row in plots:
		x1.append(float(row[0]))
		y1.append(float(row[1]))
import csv 

x10 = []
y10 = []

with open('lam_4_10.csv', 'r') as csvfile:
	plots = csv.reader(csvfile, delimiter=',')
	for row in plots:
		x10.append(float(row[0]))
		y10.append(float(row[1]))
import csv 

x100 = []
y100 = []

with open('lam_4_100.csv', 'r') as csvfile:
	plots = csv.reader(csvfile, delimiter=',')
	for row in plots:
		x100.append(float(row[0]))
		y100.append(float(row[1]))

with PdfPages('MSD_narrow.pdf') as pdf:
		
	plt.style.use('grayscale')

	plt.plot(x1,y1, label = '1')
	plt.plot(x10,y10, label = '10')
	plt.plot(x100,y100, label = '100')

	plt.ylim([0,1.19])
	plt.xlim(xmin=0)
	
	plt.xlabel('Time (seconds)')
	plt.ylabel('MSD ($\AA^{2}$)')

	plt.legend(loc=2,frameon=False)
	
	plt.tight_layout()
	pdf.savefig()
	plt.close()